<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="en" class="no-js">
    <!--<![endif]-->
    <?php include "head.php" ?>
    <body>
        <!-- Header-->
        <?php include 'inc_header.php'; ?>
        <!-- End header -->
        <!-- Slide -->
        <section>
            <div class="revolution-container">
                <div class="revolution">
                    <ul class="list-unstyled">	
                        <!-- SLIDE  -->
                        <!-- Option1 -->
                        <li data-transition="fade" data-slotamount="4" data-masterspeed="700" >
                            <!-- MAIN IMAGE -->
                            <img src="img/girl-in-the-gym2.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <div class="tp-caption skewfromrightshort customout"
                                data-x="5"
                                data-y="230"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="300"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power4.easeIn"
                                data-captionhidden="on"
                                style="z-index: 2; border-radius: 5px; width: 650px; height: 230px; background-color: rgba(50,50,50,.5);">
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="215"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                                <h1 style="font-family: Helvetica, serif; font-size:90px; font-weight: bold; color: #eb2c33">  MasterGym </h1>
                            </div>
                            <div class="tp-caption skewfromrightshort customout"
                                 data-x="20"
                                 data-y="315"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="700"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 4">
                                <h1 style="font-family: Helvetica, serif; font-size:50px; font-weight: 400; color: #fff">  70% OFF </h1>
                            </div>
                            <div class="tp-caption customin customout hidden-xs"
                                 data-x="20"
                                 data-y="410"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500"
                                 data-start="1000"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power4.easeIn"
                                 data-captionhidden="on"
                                 style="z-index: 2">
                                <a href="#" class="btn-home">Save Now!</a>
                            </div>
                            
                        </li>
                        <!-- Option2 -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="img/pizza-menu.jpeg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                                <!-- LAYERS -->
                                    <div class="tp-caption skewfromrightshort customout"
                                         data-x="5"
                                         data-y="230"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="300"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 2; border-radius: 5px; width: 650px; height: 230px; background-color: rgba(50,50,50,.5);">
                                    </div>
                                    <div class="tp-caption skewfromrightshort customout"
                                         data-x="20"
                                         data-y="215"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 3">
                                        <h1 style="font-family: Helvetica, serif; font-size:90px; font-weight: bold; color: #eb2c33">  Fast & Funny </h1>
                                    </div>
                                    <div class="tp-caption skewfromrightshort customout"
                                         data-x="20"
                                         data-y="315"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="700"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 5">
                                        <h1 style="font-family: Helvetica, serif; font-size:50px; font-weight: 400; color: #fff"> Dinner 20% OFF </h1>
                                    </div>
                                    <div class="tp-caption customin customout hidden-xs"
                                         data-x="20"
                                         data-y="410"
                                         data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="1000"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 4">
                                        <a href="#" class="btn-home">Shop All</a>
                                    </div>
                                
                        </li>
                        <!-- Option3 -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="img/desayuno.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <div class="tp-caption skewfromrightshort customout"
                                         data-x="5"
                                         data-y="230"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="300"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 2; border-radius: 5px; width: 650px; height: 230px; background-color: rgba(50,50,50,.5);">
                                    </div>
                                    <div class="tp-caption skewfromrightshort customout"
                                         data-x="20"
                                         data-y="215"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 3">
                                        <h1 style="font-family: Helvetica, serif; font-size:90px; font-weight: bold; color: #eb2c33">  GB café </h1>
                                    </div>
                                    <div class="tp-caption skewfromrightshort customout"
                                         data-x="20"
                                         data-y="315"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="700"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 5">
                                        <h1 style="font-family: Helvetica, serif; font-size:50px; font-weight: 400; color: #fff"> Coffe 30% OFF </h1>
                                    </div>
                                    <div class="tp-caption customin customout hidden-xs"
                                         data-x="20"
                                         data-y="410"
                                         data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="1000"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="500"
                                         data-endeasing="Power4.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 4">
                                        <a href="#" class="btn-home">Buy</a>
                                    </div>
                        </li>
                        <!-- EndSlide -->
                    </ul>
                    <div class="revolutiontimer"></div>
                </div>
            </div>
        </section>
        
        <section>
            <div>
                <div class="container">
                    <div class="row">
                        <aside class="col-md-3">
                            <div class="main-category-block ">
                                <div class="main-category-title">
                                    <i class="fa fa-list"></i> Category

                                </div>
                                <div class="main-category-items">
                                    <div class="widget-block">
                                        <ul class="list-unstyled ul-side-category">
                                            <li><a href=""><i class="fa fa-angle-right"></i> Cars / 2</a>
                                                <ul class="sub-category">
                                                    <li><a href="">Cars / 2</a>
                                                        <ul class="sub-category">
                                                            <li><a href="#">Dress 1</a></li>
                                                            <li><a href="#">Dress 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="">Clothes / 2</a>
                                                        <ul class="sub-category">
                                                            <li><a href="#">Shirt 1</a></li>
                                                            <li><a href="#">Shirt 2</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href=""><i class="fa fa-angle-right"></i> Travels / 2</a>
                                                <ul class="sub-category">
                                                    <li><a href="">Dress / 2 </a>
                                                        <ul class="sub-category">
                                                            <li><a href="#">Dress 1</a></li>
                                                            <li><a href="#">Dress 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="">Sport / 2</a>
                                                        <ul class="sub-category">
                                                            <li><a href="#">Shirt 1</a></li>
                                                            <li><a href="#">Shirt 2</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-angle-right"></i> Technology / 0</a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-angle-right"></i> Free Time / 0</a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-angle-right"></i> Sport / 0</a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-angle-right"></i> Laptop / 0</a>
                                            </li>

                                        </ul>

                                    </div>
                                </div>
                            </div>

                            <article class="product light last-sale">
                                <figure class="figure-hover-overlay">                                                                        
                                    <a href="#"  class="figure-href"></a>
                                    <div class="product-sale">Save <br> 17%</div>
                                    <div class="product-sale-time"><p class="time"></p></div>
                                    
                                    <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                    <img src="img/preview/product/nike400x500.jpg" class="img-overlay img-responsive" alt="">
                                    <img src="img/preview/product/nike400x500.jpg" class="img-responsive" alt="">
                                </figure>
                                <div class="product-caption">
                                    <div class="block-name">
                                        <a href="#" class="product-name">Product name</a>
                                        <p class="product-price"><span>$330</span> $320.99</p>

                                    </div>
                                    <div class="product-cart">
                                        <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                    </div>
                                </div>

                            </article>


                            <div class="widget-title">
                                <i class="fa fa-thumbs-up"></i> Bestseller
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="http://placehold.it/400x500.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8  col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review hidden-md">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="http://placehold.it/400x500" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review hidden-md">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="widget-title">
                                <i class="fa fa-tags"></i> Tags
                            </div>
                            <div class="widget-block">
                                <ul class="list-unstyled tags">
                                    <li><a href="#">men</a></li>
                                    <li><a href="#">women</a></li>
                                    <li><a href="#">top</a></li>
                                    <li><a href="#">clothes</a></li>
                                    <li><a href="#">sale</a></li>
                                    <li><a href="#">short</a></li>
                                    <li><a href="#">dresses</a></li>
                                    <li><a href="#">skirt</a></li>
                                    <li><a href="#">top</a></li>
                                </ul>
                            </div>


                        </aside>
                        <div class="col-md-9">

                            <div class="banner">
                                <a href="#">
                                    <img src="img/master_club_trainer.png" class="img-responsive" alt="">
                                </a> 
                            </div>
                            <div class="header-with-icon">
                                <i class="fa fa-tags"></i> Weekend Highlights!
                                <div class="toolbar-for-light" id="nav-summer-sale">
                                    <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                                    <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            <div id="owl-summer-sale"  class="owl-carousel">
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/shirt400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/shirt400x500.jpg" class="img-responsive" alt="">

                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                             
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/nike400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/nike400x500.jpg" class="img-responsive" alt="">

                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                             
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                            </div>

                            <div class="header-with-icon">
                                <i class="fa fa-male"></i> Latest Offers
                                <div class="toolbar-for-light" id="nav-child">
                                    <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                                    <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            <div id="owl-child"  class="owl-carousel">
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/shirt400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/shirt400x500.jpg" class="img-responsive" alt="">

                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                            <div class="product-new">new</div>
                                             
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/nike400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/nike400x500.jpg" class="img-responsive" alt="">

                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                                
                                <div class="text-center">
                                    <article class="product light wow fadeInUp">
                                        <figure class="figure-hover-overlay">                                                                        
                                            <a href="#"  class="figure-href"></a>
                                             
                                            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-overlay img-responsive" alt="">
                                            <img src="img/preview/product/samsung_400x500.jpg" class="img-responsive" alt="">
                                        </figure>
                                        <div class="product-caption">
                                            <div class="block-name">
                                                <a href="#" class="product-name">Product name</a>
                                                <p class="product-price"><span>$330</span> $320.99</p>

                                            </div>
                                            <div class="product-cart">
                                                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                            </div>
                                        </div>

                                    </article>
                                </div>
                            </div>


                            <div class="block-product-tab">
                                <div class="tab-bg"></div>
                                <div class="toolbar-for-light" id="nav-tabs2">
                                    <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                                    <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                                </div>  
                                <ul class="nav nav-pills">
                                    <li class="active"><a href="#new" data-toggle="tab">New Arrivals</a><div class="header-bottom-line"></div></li>
                                    <li><a href="#featured" data-toggle="tab" class="disabled">Featured</a><div class="header-bottom-line"></div></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active animated fadeIn" id="new">
                                        <div id="owl-new2"  class="owl-carousel">
                                            <div class="text-center">
                                                <div class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-sale">11% <br> off</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>


                                                    </div>

                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">

                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-new">new</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">

                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-sale">17% <br> off</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>

                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-new">new</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane animated fadeIn" id="featured">
                                        <div id="owl-featured2"  class="owl-carousel">

                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-new">new</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">

                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-sale">17% <br> off</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>

                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-new">new</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">

                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>
                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>

                                            <div class="text-center">
                                                <article class="product light">
                                                    <figure class="figure-hover-overlay">                                                                        
                                                        <a href="#"  class="figure-href"></a>
                                                        <div class="product-sale">7% <br> off</div>
                                                         
                                                        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                                                        <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
                                                        <img src="http://placehold.it/400x500" class="img-responsive" alt="">
                                                    </figure>
                                                    <div class="product-caption">
                                                        <div class="block-name">
                                                            <a href="#" class="product-name">Product name</a>
                                                            <p class="product-price"><span>$330</span> $320.99</p>

                                                        </div>
                                                        <div class="product-cart">
                                                            <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                                                        </div>
                                                    </div>

                                                </article>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <article class="banner">
                                        <a href="#">
                                            <img src="http://placehold.it/800x400" class="img-responsive" alt="">
                                        </a> 
                                    </article> 
                                </div>
                                <div class="col-md-6">
                                    <article class="banner">
                                        <a href="#">
                                            <img src="http://placehold.it/800x400" class="img-responsive" alt="">
                                        </a> 
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>

        </section>

        <section>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <article class="payment-service">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-thumbs-up"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">Safe Payments</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article class="payment-service">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-truck"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">Free shipping</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article class="payment-service">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-fax"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">24/7 Support</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>



                </div>
            </div>
        </section>
        
        <?php include "footer.php" ?>
        <!-- End Section footer -->
        <script src="js/vendor/jquery.js"></script>
        <script src="js/vendor/jquery.easing.1.3.js"></script>
        <script src="js/vendor/bootstrap.js"></script>

        <script src="js/vendor/jquery.flexisel.js"></script>
        <script src="js/vendor/wow.min.js"></script>
        <script src="js/vendor/jquery.transit.js"></script>
        <script src="js/vendor/jquery.jcountdown.js"></script>
        <script src="js/vendor/jquery.jPages.js"></script>
        <script src="js/vendor/owl.carousel.js"></script>

        <script src="js/vendor/responsiveslides.min.js"></script>
        <script src="js/vendor/jquery.elevateZoom-3.0.8.min.js"></script>

        <!-- jQuery REVOLUTION Slider  -->
        <script type="text/javascript" src="js/vendor/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery.scrollTo-1.4.2-min.js"></script>

        <!-- Custome Slider  -->
        <script src="js/main.js"></script>

        <!--Here will be Google Analytics code from BoilerPlate-->
    </body>
</html>