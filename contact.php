<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="en" class="no-js">
    <!--<![endif]-->
    <?php include "head.php" ?>
    <body class="contact-bg">
        <!-- Header-->
        <?php include 'inc_header.php'; ?>
        <!-- End header -->
        <section>
            <div class="second-page-container">
                <div class="block-breadcrumb">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Page</a></li>
                            <li class="active">Contact</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="container">
                        <div class="header-for-light">
                            <h1 class="wow fadeInRight animated" data-wow-duration="1s"><span>Contact</span> Information </h1>
                        </div>
                        <div class="row">
                            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="block-form box-border wow fadeInLeft animated" data-wow-duration="1s">
                                    <h3><i class="fa fa-thumb-tack"></i>Our Address</h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.                                    </p>
                                    <hr>
                                    <h3><i class="fa fa-envelope-o"></i>Send Message</h3>
                                    <form method="post" action="#">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputFirstName" class="control-label">Name:<span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="text" class="form-control" id="inputFirstName">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputLastName" class="control-label">Email:<span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="email" class="form-control" id="inputLastName">
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="inputSubject" class="control-label">Subject:<span class="text-error">*</span></label>
                                                    <div>
                                                        <input type="text" class="form-control" id="inputSubject">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="inputText" class="control-label">Message:<span class="text-error">*</span></label>
                                                    <div>
                                                        <textarea class="form-control" id="inputText"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <input type="submit" class="btn-default-1" value="Submit">
                                    </form>
                                </div>
                            </article>
                            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="block-form box-border wow fadeInRight animated" data-wow-duration="1s">
                                    <h3> <i class="fa fa-adn"></i>Map</h3>
                                    <hr>
                                    <div class="google-map">
                                        
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3142.6902102544072!2d-57.5541419!3d-38.031001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9584dc18c57c7a51%3A0xdec18f454522b966!2sFalucho+2200%2C+B7600FQR+Mar+del+Plata%2C+Buenos+Aires!5e0!3m2!1sen!2sar!4v1480539350523" style="overflow:hidden;height:100%;width:100%;" frameborder="0" ></iframe>
                                    </div>

                                </div>
                            </article>

                        </div>
                    </div>
                </div>
            </div> 
        </section>

        <section>
            <div class="block color-scheme-white-90">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <article class="payment-service">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-thumbs-up"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">Safe Payments</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article class="payment-service">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-truck"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">Free shipping</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article class="payment-service">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-fax"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">24/7 Support</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>



                </div>
            </div>
        </section>

        <?php include "footer.php" ?>
        <!-- End Section footer -->
        <script src="js/vendor/jquery.js"></script>
        <script src="js/vendor/jquery.easing.1.3.js"></script>
        <script src="js/vendor/bootstrap.js"></script>

        <script src="js/vendor/jquery.flexisel.js"></script>
        <script src="js/vendor/wow.min.js"></script>
        <script src="js/vendor/jquery.transit.js"></script>
        <script src="js/vendor/jquery.jcountdown.js"></script>
        <script src="js/vendor/jquery.jPages.js"></script>
        <script src="js/vendor/owl.carousel.js"></script>

        <script src="js/vendor/responsiveslides.min.js"></script>
        <script src="js/vendor/jquery.elevateZoom-3.0.8.min.js"></script>

        <!-- jQuery REVOLUTION Slider  -->
        <script type="text/javascript" src="js/vendor/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery.scrollTo-1.4.2-min.js"></script>

        <!-- Custome Slider  -->
        <script src="js/main.js"></script>

        <!--Here will be Google Analytics code from BoilerPlate-->
    </body>
</html>